## Installation
* Ruby version => 2.7.1
* Rails version => 6.0.3.3

* Database creation (MySQL)
    - create database sehatq;

* Database initialization
    - rake db:migrate
    - rake db:seed

* How to run the test suite
    - bundle install
    - bundle exec rspec
    - ./run.sh

## Endpoint
**Register :**  
curl --location --request POST 'http://localhost:3000/auth/register' \
--header 'Content-Type: application/json' \
--data-raw '{
    "name": "Ado Pabianko",
    "email": "adopabianko@gmail.com",
    "phone_number": "081283781369",
    "birth_date": "06-01-1992",
    "address": "Jalan Gotong Royong 1",
    "password": "secret",
    "repeat_password": "secret"
}'

**Login :**  
curl --location --request POST 'http://localhost:3000/auth/login' \
--header 'Content-Type: application/json' \
--data-raw '{
    "email": "adopabianko@gmail.com",
    "password": "secret"
}' 

**Get All Doctor :**  
curl --location --request GET 'http://localhost:3000/doctor'

**Get Doctor By ID :**  
curl --location --request GET 'http://localhost:3000/doctor/1'

**Schedule Booking :**  
curl --location --request POST 'http://localhost:3000/schedule/booking' \
--header 'Content-Type: application/json' \
--data-raw '{
    "customer_id": 1,
    "doctor_id": 1,
    "schedule_id": 1,
    "is_faskes": 0,
    "payment_type": "gopay",
    "notes": "Lorem Ipsum is simply dummy text of the printing and typesetting industry"
}'

**List Schedule Booking :**  
curl --location --request GET 'http://localhost:3000/schedule/list_booking'