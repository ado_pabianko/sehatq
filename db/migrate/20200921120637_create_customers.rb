class CreateCustomers < ActiveRecord::Migration[6.0]
  def change
    create_table :customers do |t|
      t.string :name, null:false
      t.string :email, null:false, :unique => true
      t.string :phone_number, null:false
      t.date :birth_date, null:false
      t.text :address, null:false
      t.text :profile_picture, null:true
      t.string :password, null:false

      t.timestamps
    end
  end
end
