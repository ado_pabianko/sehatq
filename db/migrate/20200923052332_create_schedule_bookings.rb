class CreateScheduleBookings < ActiveRecord::Migration[6.0]
  def change
    create_table :schedule_bookings do |t|
      t.integer :customer_id
      t.integer :doctor_id
      t.integer :schedule_id
      t.boolean :is_faskes
      t.string :payment_type
      t.text :notes

      t.timestamps
    end
  end
end
