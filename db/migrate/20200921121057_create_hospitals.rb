class CreateHospitals < ActiveRecord::Migration[6.0]
  def change
    create_table :hospitals do |t|
      t.string :name, null:false
      t.text :address, null:false
      t.text :profile_pictures

      t.timestamps
    end
  end
end
