class CreateDoctors < ActiveRecord::Migration[6.0]
  def change
    create_table :doctors do |t|
      t.string :name, null:false
      t.integer :doctor_category_id, null:false
      t.integer :hospital_id, null:false
      t.text :profile_pictures

      t.timestamps
    end
  end
end
