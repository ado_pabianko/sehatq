class CreateSchedules < ActiveRecord::Migration[6.0]
  def change
    create_table :schedules do |t|
      t.integer :doctor_id, null:false
      t.integer :day, null:false
      t.time :start_time, null:false
      t.time :end_time, null:false
      t.integer :total, null:false, default: 0

      t.timestamps
    end
  end
end
