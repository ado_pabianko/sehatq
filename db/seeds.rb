# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rails db:seed command (or created alongside the database with db:setup).
#
# Examples:
#
#   movies = Movie.create([{ name: 'Star Wars' }, { name: 'Lord of the Rings' }])
#   Character.create(name: 'Luke', movie: movies.first)

# Hospital.destroy_all
ActiveRecord::Base.connection.execute("truncate table hospitals")
Hospital.create!([
    {
        name: "RS Mata Nusantara Lebak Bulus (KMN)",
        address: "Cilandak, Jakarta Selatan",
        profile_pictures: "https://www.thailandmedical.news/uploads/hospital/5dc96be6ec460_rural_hospital_access.jpg"
    },
    {
        name: "RS St. Carolus",
        address: "Senen, Jakarta Pusat",
        profile_pictures: "https://www.thailandmedical.news/uploads/hospital/5dc96be6ec460_rural_hospital_access.jpg"
    },
    {
        name: "RS Cinta Kasih Tzu Chi",
        address: "Cengkareng, Jakarta Barat",
        profile_pictures: "https://www.thailandmedical.news/uploads/hospital/5dc96be6ec460_rural_hospital_access.jpg"
    },
    {
        name: "RS Gading Pluit",
        address: "Kelapa Gading, Jakarta Utara",
        profile_pictures: "https://www.thailandmedical.news/uploads/hospital/5dc96be6ec460_rural_hospital_access.jpg"
    },
    {
        name: "RS RK Charitas",
        address: "Ilir Timur I, Palembang",
        profile_pictures: "https://www.thailandmedical.news/uploads/hospital/5dc96be6ec460_rural_hospital_access.jpg"
    }
])

p "Created #{Hospital.count} Hospital"

# DoctorCategory.destroy_all
ActiveRecord::Base.connection.execute("truncate table doctor_categories")
DoctorCategory.create!([
    {
        name: "Dokter Spesialis Mata",
    },
    {
        name: "Psikolog",
    },
    {
        name: "Ahli Gizi",
    },
    {
        name: "Fisioterapis",
    },
    {
        name: "Dokter Spesialis Anak",
    },
])

p "Created #{DoctorCategory.count} Doctor Category"

# Doctor.destroy_all
ActiveRecord::Base.connection.execute("truncate table doctors")
Doctor.create!([
    {
        name: "dr. Yulinda Indarnila Soemiatno, Sp.M",
        doctor_category_id: 1,
        hospital_id: 1,
        profile_pictures: "http://www.venmond.com/demo/vendroid/img/avatar/big.jpg",
    },
    {
        name: "Yanthi Andriani, S.Psi",
        doctor_category_id: 2,
        hospital_id: 2,
        profile_pictures: "http://www.venmond.com/demo/vendroid/img/avatar/big.jpg",
    },
    {
        name: "Veronika Sisilia A. Utama, S.Gz",
        doctor_category_id: 3,
        hospital_id: 3,
        profile_pictures: "http://www.venmond.com/demo/vendroid/img/avatar/big.jpg",
    },
    {
        name: "Tri Wahyu Woro Wibowo, Amd.TW",
        doctor_category_id: 4,
        hospital_id: 4,
        profile_pictures: "https://preview.keenthemes.com/metronic-v4/theme/assets/pages/media/profile/profile_user.jpg",
    },
    {
        name: "Prof. dr. Zarkasih Anwar, Sp.A(K)",
        doctor_category_id: 5,
        hospital_id: 5,
        profile_pictures: "https://preview.keenthemes.com/metronic-v4/theme/assets/pages/media/profile/profile_user.jpg",
    }
])

p "Created #{Doctor.count} Doctor"

# Schedule.destroy_all
ActiveRecord::Base.connection.execute("truncate table schedules")
Schedule.create!([
    {
        doctor_id: 1,
        day: 2,
        start_time: "09:00",
        end_time: "12:00",
        total: 0,
    },
    {
        doctor_id: 1,
        day: 3,
        start_time: "15:00",
        end_time: "17:30",
        total: 0,
    },
    {
        doctor_id: 1,
        day: 6,
        start_time: "09:00",
        end_time: "12:00",
        total: 0,
    },
    {
        doctor_id: 2,
        day: 1,
        start_time: "08:00",
        end_time: "16:00",
        total: 0,
    },
    {
        doctor_id: 2,
        day: 2,
        start_time: "08:00",
        end_time: "16:00",
        total: 0,
    },
    {
        doctor_id: 2,
        day: 3,
        start_time: "08:00",
        end_time: "16:00",
        total: 0,
    },
    {
        doctor_id: 1,
        day: 5,
        start_time: "08:00",
        end_time: "16:00",
        total: 0,
    },
    {
        doctor_id: 1,
        day: 6,
        start_time: "08:00",
        end_time: "16:00",
        total: 0,
    },
    {
        doctor_id: 1,
        day: 2,
        start_time: "09:00",
        end_time: "12:00",
        total: 0,
    },
    {
        doctor_id: 4,
        day: 1,
        start_time: "09:00",
        end_time: "12:00",
        total: 0,
    },
    {
        doctor_id: 4,
        day: 3,
        start_time: "09:00",
        end_time: "12:00",
        total: 0,
    },
    {
        doctor_id: 4,
        day: 3,
        start_time: "09:00",
        end_time: "12:00",
        total: 0,
    },
    {
        doctor_id: 5,
        day: 1,
        start_time: "09:00",
        end_time: "12:00",
        total: 0,
    },
    {
        doctor_id: 5,
        day: 3,
        start_time: "09:00",
        end_time: "11:30",
        total: 0,
    },
    {
        doctor_id: 5,
        day: 5,
        start_time: "09:00",
        end_time: "11:30",
        total: 0,
    }
])

p "Created #{Schedule.count} Schedule"