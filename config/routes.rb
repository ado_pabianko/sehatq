Rails.application.routes.draw do
  post 'schedule/booking', to: 'schedule#booking'
  get 'schedule/list_booking', to: 'schedule#list_booking'
  get 'doctor', to: 'doctor#index'
  get 'doctor/:id', to: 'doctor#doctor_by_id'
  post 'auth/register'
  post 'auth/login'
  # For details on the DSL available within this file, see https://guides.rubyonrails.org/routing.html
end
