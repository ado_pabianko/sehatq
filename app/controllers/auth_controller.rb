# require 'bcrypt'
class AuthController < ApplicationController
  def register
    rp = register_params
    rp[:password] = BCrypt::Password.create(rp[:password])
    
    @customer = Customer.create!(rp)
    render json: {
      status: "Ok",
      message: "Anda berhasil melakukan registrasi",
      data: @customer
    }
  end

  def login
    lp = login_params

    @customer = Customer.find_by(email: lp[:email])

    if (@customer)
      if (BCrypt::Password.new(@customer.password) == lp[:password])
        render json: {
          status: "Ok",
          mesage: "Anda berhasil melakukan login",
          data: @customer
        }
      else
        render json: {
          status: "Error",
          message: "Email / password yang anda masukkan salah"
        }, status: 404
      end
    else
      render json: {
        status: "Error",
        message: "Email / password yang anda masukkan salah"
      }, status: 404
    end
  end

  private
    def register_params
      params.permit(:name, :email, :phone_number, :birth_date, :address, :password)
    end

    def login_params
      params.permit(:email, :password)
    end
end
