class DoctorController < ApplicationController
  def index
    @doctors = Doctor.select('id, name, profile_pictures, hospital_id, doctor_category_id').order("name asc")

    @data = @doctors.to_json(
      :include =>
        {
          :hospital => {:except => [:created_at, :updated_at]},
          :doctor_category => {:except => [:created_at, :updated_at]},
          :schedule => {:except => [:created_at, :updated_at]},
        }
    )

    @data = ActiveSupport::JSON.decode(@data)

    render json: {
      status: 'Ok',
      message: 'List doctors',
      data: @data
    }
  end

  def doctor_by_id
    @doctor = Doctor.select('id, name, profile_pictures, hospital_id, doctor_category_id').where(:id => doctor_by_id_params[:id]).order("name asc")

    @data = @doctor.to_json(
      :include =>
        {
          :hospital => {:except => [:created_at, :updated_at]},
          :doctor_category => {:except => [:created_at, :updated_at]},
          :schedule => {:except => [:created_at, :updated_at]},
        }
    )

    @data = ActiveSupport::JSON.decode(@data)

    render json: {
      status: 'Ok',
      message: 'Data doctor',
      data: @data
    }
  end

  private

  def doctor_by_id_params
    params.permit(:id)
  end
end
