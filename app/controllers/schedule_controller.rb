class ScheduleController < ApplicationController
  def booking
    @booking = ScheduleBooking.create!(booking_params)

    @booking_data = ScheduleBooking
      .select("
        schedule_bookings.id,
        customers.name as customer_name,
        doctors.name as doctor_name,
        concat(schedules.start_time,' - ',schedules.end_time) as time
      ")
      .joins(:customer)
      .joins(:doctor)
      .joins(:schedule)
      .where(:id => @booking.id)
      .all

    render json: {
      status: 'Ok',
      message: 'Sukses, anda berhasil melakukan booking',
      data: @booking_data
    }
  end

  def list_booking
    @booking_data = ScheduleBooking
      .select("
        schedule_bookings.id,
        customers.name as customer_name,
        doctors.name as doctor_name,
        concat(schedules.start_time,' - ',schedules.end_time) as time
      ")
      .joins(:customer)
      .joins(:doctor)
      .joins(:schedule)
      .all

    render json: {
      status: 'Ok',
      message: 'List booking data',
      data: @booking_data
    }
  end

  private

  def booking_params
    params.permit(:customer_id, :doctor_id, :schedule_id, :is_faskes, :payment_type, :notes)
  end
end
