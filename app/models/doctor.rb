class Doctor < ApplicationRecord
    belongs_to :hospital
    belongs_to :doctor_category
    has_many :schedule
    has_many :schedule_booking
end
