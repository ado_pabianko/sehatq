class Schedule < ApplicationRecord
  belongs_to :doctor
  has_many :schedule_booking
end
