require 'rails_helper'

RSpec.describe "Auth API", type: :request do
  describe "Registration Customer" do
    it 'customer can register' do
      post '/auth/register', params: {name: 'Mantul', email: 'mantul@gmail.com', phone_number: '081283784748', birth_date: '1992-06-04', address: 'Jalan - jalan', password: BCrypt::Password.create('secret')}
 
      expect(response).to have_http_status(:success)
    end
  end

  describe "Login Customer" do
    it 'customer can login' do
      FactoryBot.create(:customer, name: 'Mantul', email: 'mantul@gmail.com', phone_number: '081283784748', birth_date: '1992-06-04', address: 'Jalan - jalan', password: BCrypt::Password.create('secret'))

      post '/auth/login', params: { email: 'mantul@gmail.com', password: 'secret'}

      expect(response).to have_http_status(:success)
    end
  end
end
