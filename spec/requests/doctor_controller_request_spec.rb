require 'rails_helper'

RSpec.describe "Doctor API", type: :request do
	describe 'GET /doctor' do
		it 'returns all doctors' do
			get '/doctor'

			expect(response).to have_http_status(:success)
		end
	end

	describe 'GET /doctor/1' do
		it 'returns doctor by id' do
			get '/doctor/1'
	
			expect(response).to have_http_status(:success)
		end
	end
end
