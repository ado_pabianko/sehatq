require 'rails_helper'

RSpec.describe "Schedule API", type: :request do
	describe 'GET /schedule/list_booking' do
		it 'returns all schedule booking' do
			get '/schedule/list_booking'
	
			expect(response).to have_http_status(:success)
		end
	end
end
