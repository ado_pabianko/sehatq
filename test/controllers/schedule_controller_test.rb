require 'test_helper'

class ScheduleControllerTest < ActionDispatch::IntegrationTest
  test "should get book" do
    get schedule_book_url
    assert_response :success
  end

  test "should get list_book" do
    get schedule_list_book_url
    assert_response :success
  end

end
